const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');

function matchesPerYear() {
  let matchData = [];
  fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
    .pipe(csv({}))
    .on('data', (data) => matchData.push(data))
    .on('end', () => {


      const resultObject = matchData.reduce((accumulator, match) => {
        const season = match.season;
        const winner = match.winner;

        if (!accumulator[season]) {
          accumulator[season] = {};
        }

        if(winner === ""){
          return accumulator;
        }

        else if (!accumulator[season][winner]) {
          accumulator[season][winner] = 1;
        }
        else {
          accumulator[season][winner]++;
        }
        return accumulator;
      }, {})


      const newDataFile = 'resultObject.json';

      fs.writeFileSync(path.join(__dirname, "../public/output/matchesWonPerYearPerTeam.json"), JSON.stringify(resultObject, null, 2));

    })
    .on('error', (error) => {
      console.error('Error:', error);
    });
}

matchesPerYear();

