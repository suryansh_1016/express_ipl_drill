const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');


function extraRunsConceded() {

    const matchData = [];

    fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
        .pipe(csv({}))
        .on('data', (data) => matchData.push(data))
        .on('end', () => {


            let deliveryData = [];

            fs.createReadStream(path.join(__dirname, '../data/deliveries.csv'))
                .pipe(csv({}))
                .on('data', (data) => deliveryData.push(data))
                .on('end', () => {



                    const result = deliveryData.reduce((accumulator, deliveryData) => {

                        const matchId = deliveryData.match_id;
                        const bowlingTeam = deliveryData.bowling_team;
                        const extraRuns = parseInt(deliveryData.extra_runs);

                        const match = matchData.find(match => match.id === matchId);

                        if (match && match.season == '2016') {
                            accumulator[bowlingTeam] = (accumulator[bowlingTeam] || 0) + extraRuns;
                        }
                        return accumulator;

                    }, {})


                    const newDataFile = 'resultObject.json';

                    fs.writeFileSync(path.join(__dirname, "../public/output/extraRunsConcededPerTeam.json"), JSON.stringify(result));
                });
        });
}

extraRunsConceded();


