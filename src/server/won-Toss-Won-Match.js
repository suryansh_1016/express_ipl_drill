const csv = require('csv-parser');
const fs = require('fs');
const path = require('path');


function winTossWinMatch() {

    const matchData = [];

    fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
        .pipe(csv({}))
        .on('data', (data) => matchData.push(data))
        .on('end', () => {


            const resultObject = matchData.reduce((accumulator, match) => {
                const tossWinner = match.toss_winner;
                const matchWinner = match.winner;

                if (tossWinner === matchWinner) {
                    if (!accumulator[tossWinner]) {
                        accumulator[tossWinner] = 1;
                    }
                    else {
                        accumulator[tossWinner]++;
                    }
                }
                return accumulator;
            }, {});



            const newDataFile = 'resultObject.json';

            fs.writeFileSync(path.join(__dirname, "../public/output/wonTossAndMatch.json"), JSON.stringify(resultObject, null, 2));

        });
}

winTossWinMatch();