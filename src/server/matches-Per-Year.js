const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');

function matchesPerYear() {
  let matchData = [];
  fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
    .pipe(csv({}))
    .on('data', (data) => matchData.push(data))
    .on('end', () => {


      const resultObject = matchData.reduce((accumulator, currentValue) => {
        const season = currentValue.season;
        if (!accumulator[season]) {
          accumulator[season] = 1;
        } else {
          accumulator[season]++;
        }

        return accumulator;

      }, {});


      const newDataFile = 'resultObject.json';

      fs.writeFileSync(path.join(__dirname, "../public/output/matchesPerYear.json"), JSON.stringify(resultObject, null, 2));

    })
    .on('error', (error) => {
      console.error('Error:', error);
    });
}

matchesPerYear();
