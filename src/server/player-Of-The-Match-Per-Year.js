const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');

function playerOfTheMatchPerYear() {
    let matchData = [];
    fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
        .pipe(csv({}))
        .on('data', (data) => matchData.push(data))
        .on('end', () => {

            let resultObject = matchData.reduce((acc, matchData) => {
                let season = matchData.season;
                let playerOfMatch = matchData.player_of_match;

                if (!acc[season]) {
                    acc[season] = {};
                }
                if (!acc[season][playerOfMatch]) {
                    acc[season][playerOfMatch] = 1;
                }
                else {
                    acc[season][playerOfMatch]++;
                }
                return acc;

            }, {});

            let player = Object.keys(resultObject).reduce((acc, season) => {
                let maxPlayer = { player: '', count: 0 };

                acc[season] = Object.keys(resultObject[season]).reduce((acc1, playerOfMatch) => {
                    const count = resultObject[season][playerOfMatch];

                    if (count > maxPlayer.count) {
                        maxPlayer = { player: playerOfMatch, count };
                    }

                    return maxPlayer;
                }, {});

                return acc;
            }, {});




            const newDataFile = 'resultObject.json';

            fs.writeFileSync(path.join(__dirname, "../public/output/playerOfTheMatchPerYear.json"), JSON.stringify(player, null, 2));

        })
        .on('error', (error) => {
            console.error('Error:', error);
        });
}

playerOfTheMatchPerYear();
