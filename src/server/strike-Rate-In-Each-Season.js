const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');

function strikeRateEachSeason() {
  let matches = [];
  let deliveries = [];
  fs.createReadStream(path.join(__dirname, '../data/matches.csv'))
    .pipe(csv({}))
    .on('data', (data) => matches.push(data))
    .on('end', () => {


      fs.createReadStream(path.join(__dirname, '../data/deliveries.csv'))
        .pipe(csv({}))
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {


          const batsmanData = deliveries.reduce((delivery, currDelivery) => {
            const matchId = currDelivery.match_id;
            const batsman = currDelivery.batsman;
            const runs = parseInt(currDelivery.batsman_runs);

            if (runs > 0) {
              delivery[matchId] = delivery[matchId] || {};
              delivery[matchId][batsman] = delivery[matchId][batsman] || { runs: 0, balls: 0 };
              delivery[matchId][batsman].runs += runs;
              delivery[matchId][batsman].balls += 1;
            }

            return delivery;
          }, {});

          const result = matches.reduce((acc, curr) => {
            const matchId = curr.id;

            const season = curr.season;

            if (batsmanData[matchId]) {
              acc[season] = acc[season] || {};

              Object.entries(batsmanData[matchId]).forEach(([batsman, { runs, balls }]) => {
                const strikeRate = (runs / balls) * 100;
                acc[season][batsman] = strikeRate;
              });
            }
            return acc;
          }, {})


          fs.writeFileSync(path.join(__dirname, "../public/output/strikeRate.json"), JSON.stringify(result, null, 2));

        })
        .on('error', (error) => {
          console.error('Error:', error);
        });
    })
    .on('error', (error) => {
      console.error('Error:', error);
    });
}
strikeRateEachSeason();