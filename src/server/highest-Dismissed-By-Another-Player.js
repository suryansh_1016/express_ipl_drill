const csv = require('csv-parser');
const path = require('path');
const fs = require('fs');


function highestDismissed() {
    let deliveries = [];
    fs.createReadStream(path.join(__dirname, '../data/deliveries.csv'))
        .pipe(csv({}))
        .on('data', (data) => deliveries.push(data))
        .on('end', () => {

            const dismissals = deliveries.reduce((accumulator, delivery) => {
                const dismissedPlayer = delivery.player_dismissed;
                const bowler = delivery.bowler;

                if (dismissedPlayer && bowler) {
                    const key = `${dismissedPlayer}-${bowler}`;
                    accumulator[key] = (accumulator[key] || 0) + 1;
                }
                return accumulator;
            }, {});

            const maxDismissals = Object.keys(dismissals).reduce((maxKey, currentKey) => {
                return dismissals[currentKey] > dismissals[maxKey] ? currentKey : maxKey;
            }, Object.keys(dismissals)[0]);
            console.log(maxDismissals);
            const result = {};
            result[maxDismissals] = dismissals[maxDismissals]


            fs.writeFileSync(path.join(__dirname, "../public/output/dismissedByOther.json"), JSON.stringify(result, null, 2));
        })
        .on('error', (error) => {
            console.error('Error:', error);
        });
}
highestDismissed();
