const express = require('express');
const path = require('path');


const app = express();

app.use(express.static(path.join(__dirname, '..', 'public')));

app.get('/', (req, res) => {
    res.send('index.html');
})



app.get('/matches-per-year', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'matches_per_year.html'))
});

app.get('/matches-won-per-year-per-team', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'matches_won_per_year_per_team.html'))
});

app.get('/won-toss-and-match', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'won_toss_and_match.html'))
});

app.get('/best-economy-in-superover', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'best_economy_in_superover.html'))
});

app.get('/top-10-economical-bowler', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'top_10_economical_bowlers.html'))
});

app.get('/player-of-match', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'player_of_match.html'))
});

app.get('/top-dismissed', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'top_dismissed.html'))
});

app.get('/extra-runs', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'extra_runs.html'))
});

app.get('/strike-rate', (req, res) => {
    res.sendFile(path.join(__dirname, '..', 'public', 'html', 'strike_rate.html'))
});


port = 3000;

app.listen(port, () => {
    console.log(" server stated successfully! ");
});