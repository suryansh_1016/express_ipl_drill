async function getData() {
  const res = await fetch("../output/highestNumberOfPlayerOfMatch.json")
  const data = await res.json();


  const seriesData = Object.entries(data).map((element) => {
    return {
      name: element[0],
      data: [
        {
          name: element[1].maxPlayer,
          y: element[1].count
        }
      ]
    }
  })

  Highcharts.chart('player-of-match', {
    chart: {
      type: "bar",
    },
    title: {
      text: "Highest Player Of Match",
    },
    xAxis: {
      categories: ["Players"],
    },
    yAxis: {
      title: {
        text: "Number of Player of Matches"
      }
    },
    series: seriesData
  })

}

getData();

