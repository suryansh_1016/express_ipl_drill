
async function getData() {
    const response = await fetch('../output/top10EconomicalBowlers.json');
    let data = await response.json();

    const seriesData = data.map((element) => {
        const bowler = element.bowler;
        const economy = element.economyRate;
        return {
            name: bowler,
            data: [economy]
        }
    })

    Highcharts.chart("top-10-economical-bowlers-charts", {
        chart: {
            type: "column",
        },
        title: {
            text: "Top 10 Economical Bowlers - 2015",
        },
        xAxis: {
            categories: ["Bowler"]
        },
        yAxis: {
            title: {
                text: "Economy Rate",
            }
        },
        series: seriesData
    })
}

getData();