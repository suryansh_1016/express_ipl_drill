async function getData() {
  const res = await fetch('../output/bestEconomyInSuperOver.json');
  let data = await res.json();

  const years = Object.keys(data);
  const matches = Object.values(data);


  Highcharts.chart('best-economy-in-super-over', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Best Economy In Super Over'
    },
    xAxis: {
      categories: years,
      title: {
        text: 'bowler'
      }
    },
    yAxis: {
      title: {
        text: 'Economy'
      }
    },
    series: [{
      name: 'Year',
      data: matches
    }]
  });

}

getData();

