async function getData() {
    const res = await fetch('../output/extraRunsConcededPerYear.json');
    let data = await res.json();


    const years = Object.keys(data);
    const matches = Object.values(data);


    Highcharts.chart('extra-runs-chart', {
        chart: {
            type: 'column' // Set chart type to column
        },
        title: {
            text: 'Extra Runs Conceded Per Team'
        },
        xAxis: {
            categories: years,
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            title: {
                text: 'Runs'
            }
        },
        series: [{
            name: 'Teams',
            data: matches
        }]
    });


}

getData();

