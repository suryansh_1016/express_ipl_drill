async function getData() {
    const res = await fetch('../output/wonTossAndMatch.json');
    let data = await res.json();


    const years = Object.keys(data);
    const matches = Object.values(data);



    Highcharts.chart('won-toss-and-match-chart', {
        chart: {
            type: 'column' // Set chart type to column
        },
        title: {
            text: 'Won Toss Also The Match'
        },
        xAxis: {
            categories: years,
            title: {
                text: 'Teams'
            }
        },
        yAxis: {
            title: {
                text: 'Number of Matches'
            }
        },
        series: [{
            name: 'Teams',
            data: matches
        }]
    });


}

getData();

