async function getData() {
    const res = await fetch('../output/matchesWonPerYearPerTeam.json');
    let data = await res.json();

    const seriesData = Object.entries(data).map((entry) => {
        const year = entry[0];
        const teamsData = entry[1];

        const result = Object.entries(teamsData).map(([team , matches]) =>
        [team , matches]);

        return {
            name: year,
            data: result
        };
    });

    const teams = Object.keys(data[Object.keys(data)[0]]);
    const chartCategories = teams;

    Highcharts.chart("matches-won-per-team-per-year-chart", {
        chart: {
            type: "column",
        },
        title: {
            text: "Matches Won Per Team Per Year",
        },
        xAxis: {
            type: "category",
            title: {
text:"Team"
            }
        },
        yAxis: {
            title: {
                text: "Number of matches won",
            },
        },
        series: seriesData,
    });

}

getData();