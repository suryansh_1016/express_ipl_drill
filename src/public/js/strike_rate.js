async function getData() {
    const response = await fetch('../output/strikeRate.json');
    let data = await response.json();

    const chartsContainer = document.getElementById('strike-rate-of-batsman');


    Object.entries(data).forEach((element, index) => {
        const year = element[0];
        const batsman = element[1];

        const chartDiv = document.createElement('div');
        chartDiv.id = `strike-rate-chart-${index}`;

        chartsContainer.appendChild(chartDiv);
        Highcharts.chart(`strike-rate-chart-${index}`, {
            chart: {
                type: "column",
            },
            title: {
                text: `Strike Rate Of Batsman - ${year}`,
            },
            xAxis: {
                categories: Object.keys(batsman),
                title: {
                    text: "Year"
                }
            },
            yAxis: {
                title: {
                    text: "Strike Rate",
                }
            },
            series: [{
                name: `Strike rate in year ${year}`,
                data: Object.values(batsman),
            }]
        });
    });
}

getData();