async function getData() {
  const res = await fetch('../output/matchesPerYear.json');
  let data = await res.json();


  const years = Object.keys(data);
  const matches = Object.values(data);

  
  Highcharts.chart('matches-per-year-chart', {
    chart: {
      type: 'column' // Set chart type to column
    },
    title: {
      text: 'Number of Matches Over Years'
    },
    xAxis: {
      categories: years,
      title: {
        text: 'Year'
      }
    },
    yAxis: {
      title: {
        text: 'Number of Matches'
      }
    },
    series: [{
      name: 'Year',
      data: matches
    }]
  });


}

getData();

