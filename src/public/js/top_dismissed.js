async function getData() {
    const res = await fetch("../output/dismissedByOther.json");
    let data = await res.json();

    const seriesData = Object.entries(data).map((element) => {
        return {
            name: element[0],
            data: [element[1]]
        }
    })

    Highcharts.chart("topDismissed", {
        chart: {
            type: "column"
        },
        title: {
            text: "Top Dismissed",
        },
        xAxis: {
            categories: ["Player Dismissed - Dismissed by Player"]
        },
        yAxis: {
            title: {
                text: "Number of Dismissals",
            }
        },
        series: seriesData
    })
}

getData();